# -*- coding: utf-8 -*-

def json_list():
    import json
    f = open('eminem.json', 'r')
    json_data_eminem = json.load(f)
    f.close()
    f = open('snoop_dogg.json', 'r')
    json_data_snoop_dogg = json.load(f)
    f.close()

    all_lyrics = []
    for lines in json_data_eminem:
        s = ""
        for line in lines['lyrics']:
            s = s + line
        all_lyrics.append(s)
    for lines in json_data_snoop_dogg:
        s = ""
        for line in lines['lyrics']:
            s = s + line
        all_lyrics.append(s)

    return all_lyrics

def v():
    from sklearn.feature_extraction.text import TfidfVectorizer
    from sklearn import svm
    from sklearn import cross_validation
    import numpy as np

    doc_list = json_list()

    y = np.append(np.ones(90),np.zeros(90))

    vectorizer = TfidfVectorizer(min_df=1, max_df=50)
    X = vectorizer.fit_transform(doc_list)

    perm = np.random.permutation(X.shape[0])

    train_index = perm[0:80]
    test_index = perm[80:]
    
    X_train = X[train_index,:]
    y_train = y[train_index]
    X_test = X[test_index,:]
    y_test = y[test_index]

    clf = svm.LinearSVC()
    clf.fit(X_train, y_train)
    #predict_list = clf.predict(X_test)
    # test_data = vectorizer.transform(test_list)
    # print(clf.predict(test_data))

    return clf.score(X_test, y_test)
    

def main():
    v()
    s = 0
    for i in range(50):
        s = s + v()
    print(s/50)

main()
