# -*- coding: utf-8 -*-

import scrapy


class AzSpider(scrapy.Spider):
    name = 'az_spider'

    # エンドポイント（クローリングを開始するURLを記載する）
    start_urls = ['http://www.metrolyrics.com/snoop-dogg-lyrics.html']

    custom_settings = {
        "DOWNLOAD_DELAY": 1,
    }

    # URLの抽出処理を記載
    def parse(self, response):
        for href in response.css('.songs-table tbody tr td > a::attr(href)'):
            full_url = response.urljoin(href.extract())

            # 抽出したURLを元にRequestを作成し、ダウンロードする
            yield scrapy.Request(full_url, callback=self.parse_item)

    # ダウンロードしたページを元に、内容を抽出し保存するItemを作成
    def parse_item(self, response):

        lyrics = []
        for lyric in response.xpath('//p[@class="verse"]/text()'):
            lyrics.append(lyric.extract())

        yield {
            'title': response.css('h1::text').extract(),
            'lyrics': lyrics,
        }
